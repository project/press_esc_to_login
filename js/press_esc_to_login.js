document.onkeydown = function goToLogin(evt) {
  evt = evt || window.event

  const { loginPath } = drupalSettings.pressEscToLogin

  if (evt.key === 'Escape' && loginPath) {
    window.location.href = loginPath
  }
}
